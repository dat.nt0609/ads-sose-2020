cmake_minimum_required(VERSION 3.16)
project(2b)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(2b
        main.cpp
        Tree.cpp
        Tree.h
        TreeNode.cpp
        TreeNode.h
        catch.h
        TreeTest.cpp
        )

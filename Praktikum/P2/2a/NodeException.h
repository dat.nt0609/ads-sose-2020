//
// Created by presariohg on 24/04/2020.
//

#ifndef INC_2A_NODEEXCEPTION_H
#define INC_2A_NODEEXCEPTION_H

#include <exception>

class NodeException {
public:

struct BlankDataException : public std::exception {
    const char * what() const throw() {
        return "The requested node's data has not been set!";
    }
};


struct BlankDescriptionException : public std::exception {
    const char * what() const throw() {
        return "The requested node's description has not been set!";
    }
};


struct AgeNotSetException : public std::exception {
    const char * what() const throw() {
        return "The requested node's age has not been set!";
    }
};


struct NextNotSetException : public std::exception {
    const char * what() const throw() {
        return "The requested node's next has not been set!";
    }
};

};


#endif //INC_2A_NODEEXCEPTION_H

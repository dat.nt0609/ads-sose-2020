cmake_minimum_required(VERSION 3.16)
project(P3)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(P3
        main.cpp
        Tree.cpp
        Tree.h
        TreeNode.cpp
        TreeNode.h
        TreeNodeException.h

#        TreeTest.cpp
#        catch.h
        )

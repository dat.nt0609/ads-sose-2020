//
// Created by presariohg on 15/05/2020.
//

#ifndef P3_TREENODEEXCEPTION_H
#define P3_TREENODEEXCEPTION_H

#include <exception>

class TreeNodeException{
public:
    struct NullNodeException : public std::exception {
        const char * what() const throw() {
            return "Trying to access a null node.";
        }
    };
};

#endif //P3_TREENODEEXCEPTION_H
